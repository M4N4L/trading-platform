
# Api key = P54AJS51K7LMVGQ3

import requests
import datetime
import numpy as np

def get_price(stock, date): #récupérer le prix du stock désiré à une date donnée

    data=requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={}&outputsize=compact&apikey=P54AJS51K7LMVGQ3'.format(stock))
    data=data.json()
    data=data['Time Series (Daily)'][date]
    data = { "name" : stock, "price" : data['1. open']}
    return data

def stacks(stock): #récupérer les 100 derniers prix historiques d'un stock donné

    meta = []
    data=requests.get('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={}&outputsize=compact&apikey=P54AJS51K7LMVGQ3'.format(stock))
    data=data.json()
    data=data['Time Series (Daily)']
    for d,p in data.items():
        
        json_={"date" : d , "open" : float(p['1. open']) , "close" : float(p['4. close']) }
        meta.append(json_)
    meta = np.array(meta)
    meta = np.flipud(meta)
    meta = meta.tolist()

    return meta

#pour tester ces fonctions ci-dessus
if __name__ == '__main__':
    print(stacks("FB"))
    print(get_price("FB", "2020-07-02"))