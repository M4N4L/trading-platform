import math
import random
import numpy as np
import pandas as pd
#import logging, os
#logging.disable(logging.WARNING)
#os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
import tensorflow as tf
import matplotlib.pyplot as plt
from collections import deque #data structure

class Trader_bot():
  def __init__(self, actions, state_size,action_space=3, model_name="TraderBot"): 
    
    self.state_size = state_size #taille de l'état
    self.action_space = action_space #3
    self.actions = actions # buy , sell , hold
    self.memory = deque(maxlen=2000) # liste de taille max = 2000, s'il devient rempli les 1er inputs seront supprimé
    self.my_stocks = [] # les actions boursiers que j'ai acheté
    self.decouvert= [] # les actions boursiers que j'ai vendu à découvert
    self.model_name = model_name
     #hyperparametres
    self.gamma = 0.95  #taux de décroissance ou d'actualisation
    self.epsilon = 1.0   #taux d'exploration
    self.epsilon_final = 0.01
    self.epsilon_decay = 0.995 # diminuer le nombre d'explorations au fur et à mesure que le modele devient bon
   # self.model = self.model_builder() # créer le modele, l'initialise et le stocke dans self.model
    self.model = tf.keras.models.load_model("./trader_bot200.h5")  #appeler le modèle de neurones entrainé
    
  def model_builder(self):

    # réseau de neurones pour Deep Q Learning
    model = tf.keras.models.Sequential() 
    # couche d'entréee de taille state_size et couche cachée de 32 noeuds
    model.add(tf.keras.layers.Dense(units=32, activation='relu', input_dim=self.state_size))
    #couche cachée de 64 noeuds 
    model.add(tf.keras.layers.Dense(units=64, activation='relu'))
    #couche cachée de 128 noeuds 
    model.add(tf.keras.layers.Dense(units=128, activation='relu'))
    # Couche de sortie avec nombre d'actions : 3 nœuds
    model.add(tf.keras.layers.Dense(units=self.action_space, activation='linear')) 
    # mse=mean square error
    # lr = learning rate
    model.compile(loss='mse', optimizer=tf.keras.optimizers.Adam(lr=0.001)) 
    return model

  def select_action(self, state):
    
    if random.random() <= self.epsilon:
     # Exploration : choisit action aléatoire  
       return random.randrange(self.action_space)
      
    else:
    # Exploitation: choisit l'action en fonction de la récompense prévue(avec probabilité epsilon)
      actions = self.model.predict(state)
      action = np.argmax(actions[0])
      return action

  def train (self,batch_size):
      # training
      if len(self.memory) < batch_size:
            return # memory n'est pas encore pleine
      batch = []
      for i in range(len(self.memory) - batch_size + 1, len(self.memory)):
           batch.append(self.memory[i]) #prélever les derniers (batch_size) échantillons de la mémoire

      for state, action, reward, next_state, done in batch:
            if done: #si derniere itération,pas d'état prochain juste récompense actuelle
                target = reward
            else:
                target = reward + self.gamma * np.amax(self.model.predict(next_state)[0]) 
                
            train_target = self.model.predict(state)
            train_target[0][action] = target
            self.model.fit(state, train_target, epochs=1, verbose = 0) # verbose=0: Ne pas afficher(loss and epoch)
      
      if self.epsilon > self.epsilon_final:
         self.epsilon *= self.epsilon_decay

