import { Component, OnInit } from '@angular/core';
import { BuyStockService, BuyStock } from '../buy-stock.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from '../user.service';
import { values } from 'd3';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-comp-cards',
  templateUrl: './comp-cards.component.html',
  styleUrls: ['./comp-cards.component.css']
})
export class CompCardsComponent implements OnInit {
  
  public prediction;

  constructor(
    private ButSt: BuyStockService ,
     private _snackBar: MatSnackBar ,
     private user : UserService,
     private route : Router
    ) { }
  

  durationInSeconds = 5;
  stockers = ['FB','MSFT','AMZN']
  actions = []
  ngOnInit(): void {

    for(let stocknames in this.stockers){
      let value = this.stockers[stocknames]
      console.log(value);
      this.ButSt.getStock_price(value).subscribe((res)=>{
        this.actions.push(res);
      })
    }
    console.log(this.actions);
  }
  check;
  Predict(item) {
    let st_ock = {"name" : item.name , "price" : parseFloat(item.price) }
    const stock: BuyStock = {name : item.name , prix : parseFloat(item.price) };
    this.ButSt.predict(st_ock.name).subscribe(prediction =>{ 
      this.prediction = prediction;
      this.ButSt.getSolde().subscribe((res : any)=>{
        this.ButSt.By(stock.prix) ;
        let action = this.prediction.action;
        if (action == 1){
          this.check=1;
        }
      if(action == 0){
        this.check=0;
      }
      if(action == 2){
        this.check=2;
      }
      });
    })
  }

  Buy(item){
//sd
    const stock: BuyStock = {name : item.name , prix : parseFloat(item.price) };
    let st_ock = {"name" : item.name , "price" : parseFloat(item.price) }
    this.ButSt.buyStock(st_ock).subscribe(rs=>{
      this.ButSt.getSolde().subscribe((res : any)=>{
        let budget = res.budget;
        if (  budget >= stock.prix ) {
          this.ButSt.By(stock.prix) ;
          this._snackBar.openFromComponent(SuccesModal, {
            duration: this.durationInSeconds * 1000,
          });
        } else {
          this._snackBar.openFromComponent(FailedModal, {
            duration: this.durationInSeconds * 1000,
          });
        }

      });

    })

  }

  Sell(item) {
    let st_ock = {"name" : item.name , "price" : parseFloat(item.price) }
    const stock: BuyStock = {name : item.name , prix : parseFloat(item.price) };
    this.ButSt.sellStock(st_ock.name).subscribe(rs=>{
      this.ButSt.getSolde().subscribe((res : any)=>{
        let budget = res.budget;
        this.ButSt.By(stock.prix) ;
        this._snackBar.openFromComponent(sellModal, {
          duration: this.durationInSeconds * 1000,
        });


      });

    })


  }
  goChart(item){
    this.route.navigate(['/chart/',item])
  }
}

@Component({
  selector: 'snack-bar-component-example-snack',
  template: `<span class="example-pizza-party">
   le stock est acheté avec succès
</span>`,
  styles: [`
    .example-pizza-party {
      color: hotpink;
    }
  `],
})
export class SuccesModal {}

@Component({
  selector: 'snack-bar-component-example-snack',
  template: `<span class="example-pizza-party">
   désolé vous n'avez pas autant d'argent , recharger votre compte
</span>`,
  styles: [`
    .example-pizza-party {
      color: hotpink;
    }
  `],
})
export class FailedModal {}

@Component({
  selector: 'snack-bar-component-example-snack',
  template: `<span class="example-pizza-party">
   l'action a bien été vendu
</span>`,
  styles: [`
    .example-pizza-party {
      color: hotpink;
    }
  `],
})
export class sellModal {}
