import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompCardsComponent } from './comp-cards.component';

describe('CompCardsComponent', () => {
  let component: CompCardsComponent;
  let fixture: ComponentFixture<CompCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
