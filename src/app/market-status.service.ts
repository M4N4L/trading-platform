import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {MarketPrice} from './market-price';

import { Subject, from } from  'rxjs';
import * as socketio from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class MarketStatusService {

  private baseUrl =  'http://localhost:5000';

  constructor(private httpClient: HttpClient) { }


  get(uri : string) {
    return this.httpClient.get(`${this.baseUrl}${uri}`);
  }

  getSolde(){
    return this.get('/getBudget');
  }

  getStock_price(stock_name : string){
    return this.httpClient.get(`${this.baseUrl}/stocks/${stock_name}`);
  }

}
