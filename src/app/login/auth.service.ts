import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  private islogin = false ;

  getIslogin() {
    return this.islogin ;
  }

  Login() {
    this.islogin = true ;
  }

  Logout() {
    this.islogin = false ;
  }

}
