import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { BuyStockService } from '../buy-stock.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService , private router: Router,private buystocks : BuyStockService,private userSer : UserService) { }

  ngOnInit(): void {
    this.buystocks.getSolde().subscribe((res : any)=>{
      let budget = res.budget;
      this.userSer.firstSet(budget);
    });
  }

  Login() {
    this.auth.Login();
    this.router.navigateByUrl('/');
  }
}
