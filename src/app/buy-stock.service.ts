import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BuyStockService {
  readonly ROOT_URL: string;
  constructor(private user: UserService, private http : HttpClient) {
    this.ROOT_URL = 'http://localhost:5000';
   }


  get(uri : string) {
    return this.http.get(`${this.ROOT_URL}/${uri}`);
  }
  post(uri : string, payload : Object){
    return this.http.post(`${this.ROOT_URL}/${uri}`, payload);
  }
  patch(uri : string, payload : Object){
    return this.http.patch(`${this.ROOT_URL}/${uri}`, payload);
  }
  delete(uri : string){
    return this.http.delete(`${this.ROOT_URL}/${uri}`);
  }

  getStocks() {
    return this.get('myStocks');
  }
  By(stock:number){
    console.log("stock by",stock);
    this.user.setSolde();
  }
  buyStock(stock:any) {
    this.user.setSolde() ;
    return this.post('buy',stock);
  }
  sellStock(stock:any){
    this.user.setSolde() ;
    return this.delete(`sell/${stock}`)
  }

  getSolde(){
    return this.get('/getBudget');
  }

  login(user : any){
    return this.post('/login',user);
  }

  getStock_price(stock_name : string){
    return this.http.get(`${this.ROOT_URL}/stocks/${stock_name}`);
  }

  predict(stock:any){
    return this.get(`predict/${stock}`)
  }

}

export interface BuyStock {
  name: string;
  prix: number;
}
export interface Predict {
  action: number;
  date: number;
  profit: number;
  sharevalue: number;
  typeaction: string;

}
