import { Injectable } from '@angular/core';
import { BuyStockService } from './buy-stock.service';
import { MarketStatusService } from './market-status.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: User = {name: '' , email:"" , solde : 0};

  getUser() {
    return this.user ;
  }
  firstSet(solde:number){
    this.user.solde = solde;
  }

  setSolde() {
    this.market_ser.getSolde().subscribe((res : any)=>{
      let budget = res.budget;
      this.firstSet(budget);
      console.log("user solde",this.user.solde);
    });
  }





  constructor(private market_ser : MarketStatusService) { }
}

export interface User{
  name: string;
  email: string;
  solde: number ;

}
