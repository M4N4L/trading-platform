import { Component, OnInit } from '@angular/core';
import { BuyStockService, BuyStock } from '../buy-stock.service';

@Component({
  selector: 'app-stock-achete',
  templateUrl: './stock-achete.component.html',
  styleUrls: ['./stock-achete.component.css']
})
export class StockAcheteComponent implements OnInit {

  constructor(private BuySt: BuyStockService) { }
  stocks: BuyStock[] = [] ;

  ngOnInit(): void {

    this.BuySt.getStocks().subscribe((res)=>{

      for (let element in res) {
        let value = res[element];
        this.stocks.push({"name" : value.name, "prix" : value.price });

    }
    },(err)=>{
      console.log(err);

    });
  }

}
