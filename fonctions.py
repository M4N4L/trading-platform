import matplotlib.pyplot as plt
#import warnings
#warnings.simplefilter(action='ignore', category=Warning)
import pandas_datareader.data as web
from matplotlib import style
import datetime as dt
from DQAgent import Trader_bot
import pandas as pd
import numpy as np

style.use('ggplot')

def get_csv_file(start_date, end_date, stock_name):
  #importer les infos de stock de yahoo_finance en indiquant stock_name , dateDébut et dateFin
    df = web.DataReader(stock_name, 'yahoo', start_date, end_date)
  # les enregistrer dans fichier .csv
    df.to_csv('data.csv')

def get_data():
  # Afficher le graphe
  prices=[]
  dates=[]
  df=pd.read_csv('data.csv', parse_dates=True, index_col=0) #importer les infos du fichier .csv
  for row in df[df.columns[2]]:
    prices.append(row)  # récupérer Open price dans la liste prices
  for x in range(len(df)):
    newdate = str(df.index[x])
    newdate = newdate[0:10]
    dates.append(newdate) # récupérer les dates

  return prices,dates

def get_20Ldays_data():
  prices=[]
  dates=[]
  df=pd.read_csv('data.csv', parse_dates=True, index_col=0) #importer les infos du fichier .csv
  for row in df['Open'].tail(21) :
     prices.append(row)  # récupérer les 20 derniers Open price 
  for x in range(len(df)):
    newdate = str(df.index[x])
    newdate = newdate[0:20]
    dates.append(newdate) # réupérer les 20 dernieres dates
  dates = dates[-21:]

  return prices,dates

def predict (model, initial_budget, prices, hist, dates, my_stocks, decouvert ):
  budget = initial_budget 
  total_profit = 0
  model.my_stocks = my_stocks #initialiser liste des stocks achetés
  model.decouvert= decouvert  #initialiser liste des stocks achetés à decouvert
  data= len(prices) - hist-1
  for i in range(len(prices) - hist):

       state = np.asmatrix(np.hstack((prices[i:i+hist], budget)))
       share_value = float(prices[i + hist])
       date = dates[i + hist]
       action = model.select_action(state)
       reward = 0
      
       if action == 0 and budget >= share_value: # BUY
        # Achat de l'action vendue à découvert
          if(len(model.decouvert) > 0 and model.decouvert[0] > share_value) :  
             sell_price = model.decouvert.pop(0)
             budget -= share_value
             reward = max(sell_price - share_value, 0)
             total_profit += sell_price - share_value
             type_action="a decouvert"
             print('Trader Bot a acheté (l\'action vendue à découvert){:.2f}$'.format(share_value),'en ', date, 'Profit :{:.2f}$'.format(sell_price - share_value))
        # Achat normal
          else:  
            budget -= share_value
            model.my_stocks.append(share_value)
            type_action="normal"
            print('Trader Bot a acheté:{:.2f}$'.format(share_value),'en ', date)

       elif action == 1 : #SELL
       # Vente à découvert
          if(len(model.my_stocks) == 0) :
            model.decouvert.append(share_value)
            budget += share_value
            type_action="normal" 
            print('Trader Bot a vendu à découvert :{:.2f}$'.format(share_value),'en ', date)
       # Vente normale
          else:
            buy_price = model.my_stocks.pop(0)
            budget += share_value
            reward = max(share_value - buy_price, 0)
            total_profit += share_value - buy_price
            type_action="normal"
            print('Trader Bot a vendu :{:.2f}$'.format(share_value),'en ',date, 'Profit :{:.2f}$'.format(share_value - buy_price))
       else:
          action = 2 #HOLD
          type_action="normal"
          print("Trader Bot n'a rien fait en ", date)
      

       next_state = np.asmatrix(np.hstack((prices[i+1:i+hist+1], budget)))
       #print('next state:', next_state)
       if i == data :
          done = True
       else:
          done = False 
       model.memory.append((state, action, reward, next_state, done)) #enregistrer dans la mémoire
       
  return model.decouvert , model.my_stocks, model.memory, budget, action, type_action, total_profit, share_value, date

#Pour tester la fonction predict
if __name__ == '__main__':
    start = dt.datetime(2020, 2, 3)
    end = dt.datetime.now()
    #chargement des données
    get_csv_file(start,end,"MSFT") # stocks de microsoft
    prices,dates=get_20Ldays_data() 
    #définir les 3 actions
    actions_3 = ['Buy', 'Sell', 'Hold'] 
    # nbre de prix historique 
    hist = 20
    # définir le modele
    trader = Trader_bot(actions_3, hist+1) # taille de l'état = hist+1 (historique + budget)
    budget = 10000.0 #initialiser budget
    my_stocks=[100.0,50,200.0]
    decouvert=[53.9]
    decouvert , my_stocks, memory, budget, action, type_action, total_profit, share_value, date = predict(trader, budget, prices, hist, dates, my_stocks, decouvert)
    print("decouvert",decouvert)
    print("my_stocks",my_stocks)
    print("memory",memory)
    print("budget",budget)
    print("action",action)
    print("type_action",type_action)
    print("total_profit",total_profit)
    print("share value",share_value)
    print("date",date)



