from flask import Flask, jsonify , request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import numpy as np
from fonctions import *
import datetime as dt
import json
#import pymysql
import data as dts


app= Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
#pymysql.install_as_MySQLdb()


app.config['SQLALCHEMY_DATABASE_URI']='mysql://root:1234@localhost/trade'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db=SQLAlchemy(app)
cors = CORS(app,resources={r"/*":{"origins":"*"}})

class User(db.Model):
	__tablename__ = 'user'
	id = db.Column("id", db.Integer, primary_key=True)
	username = db.Column("username",db.String(100))
	password = db.Column("password",db.String(100))
	budget = db.Column("budget", db.Float)
	stocks = db.relationship('Bought_stock', backref='owner')

	def __init__(self, id, username, password, budget):
		self.id = id
		self.username = username
		self.password = password
		self.budget = budget

class Bought_stock(db.Model):
	id = db.Column("id", db.Integer, primary_key=True)
	name = db.Column("name",db.String(100))
	price = db.Column("price", db.Float)
	type_achat = db.Column("type_achat", db.String(30))
	user_id = db.Column("user_id",db.Integer,db.ForeignKey('user.id'))

	def __init__(self,  name, price, user_id, type_achat="normal"):
		self.name = name
		self.price = price
		self.user_id = user_id
		self.type_achat = type_achat

@app.route('/',methods=['GET'])
def test():
	return jsonify({'message':'It works!'})


@app.route('/login',methods=['POST'])
def login():
	user1 = User.query.filter_by(id=1).first()
	if request.json['username'] == user1.username and request.json['password'] == user1.password :
		return jsonify({})
	else :
		return jsonify({"message": "Acces denied!"})

@app.route('/myStocks',methods=['GET'])
def get_stocks():
	prices=[]
	names=[]
	ids=[]
	stocks = Bought_stock.query.filter_by(user_id=1).all()
	for x in stocks:
		prices.append(x.price)
	print("prices",prices)
	for x in stocks:
		names.append(x.name)
	for x in stocks:
		ids.append(x.id)
	return json.dumps([{'id':id,'name': name, 'price': price} for id,name, price in zip(ids,names, prices)])

@app.route('/myStocksById/<int:id>',methods=['GET'])
def get_stocks_by_id(id):
  stock = Bought_stock.query.filter_by(user_id=1, id=id).first()
  return jsonify({'id':stock.id, 'name': stock.name, 'price': stock.price})

@app.route('/myStocks/<string:stock_name>',methods=['GET'])
def get_one_stock(stock_name):
	prices=[]
	num=[]
	stocks = Bought_stock.query.filter_by(user_id=1, name=stock_name).all()
	for x in stocks:
		prices.append(x.price)
	for x in stocks:
		num.append(x.id)
	stock_dictionary = dict(zip(num, prices))
	return jsonify({'stocks':stock_dictionary})

@app.route('/predict/<string:stock_name>',methods=['GET'])
def predict_result (stock_name ):
	start = dt.datetime(2020, 2, 3)
	end = dt.datetime.now()
    #chargement des données
	print(stock_name)
	get_csv_file(start,end,stock_name) # stocks de microsoft
	prices,dates=get_20Ldays_data() 
    #définir les 3 actions
	actions_3 = ['Buy', 'Sell', 'Hold'] 
    # nbre de prix historique 
	hist = 20
    # définir le modele
	trader = Trader_bot(actions_3, hist+1) # taille de l'état = hist+1 (historique + budget)
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	budget = user1.budget #Budget de l'utilisateur
	my_stocks=[]
	decouvert=[]
	stocks = Bought_stock.query.filter_by(user_id=1, name=stock_name)
	for x in stocks: #Remplir les listes des actions boursières achetées (normal/ a decouvert)
		if (x.type_achat == "normal"):
			my_stocks.append(x.price)
		else:
			decouvert.append(x.price)
	decouvert , my_stocks, memory, budget, action, type_action, total_profit, share_value, date = predict(trader, budget, prices, hist, dates, my_stocks, decouvert)

	if decouvert: 
		Bought_stock.query.filter_by(user_id=1, type_achat="a decouvert").delete()
		db.session.commit()
		for x in range(len(decouvert)):
			stock1= Bought_stock(name=stock_name, price=decouvert.pop(0), type_achat="a decouvert", user_id=1)
			db.session.add(stock1)
			db.session.commit()
	if my_stocks:
		Bought_stock.query.filter_by(user_id=1, type_achat="normal").delete()
		db.session.commit()
		for x in range(len(my_stocks)):
			stock2= Bought_stock(name=stock_name, price=my_stocks.pop(0), type_achat="normal", user_id=1)
			db.session.add(stock2)
			db.session.commit()

	updated = User.query.filter_by(id=1).update({User.budget : budget}) #Modifier le budget dans BD
	db.session.commit()
	
	return jsonify({'action':action, 'typeaction': type_action, 'profit': total_profit, 'sharevalue': share_value,
	       'date': date})


@app.route('/sell/<string:stock_name>',methods=['DELETE'])
def sell(stock_name):
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	stock = Bought_stock.query.filter_by(user_id=1, name=stock_name).first()
	user1.budget = user1.budget + stock.price
	db.session.delete(stock)
	db.session.commit()
	return jsonify({})

@app.route('/sellById/<int:id>',methods=['DELETE'])
def sell_by_id(id):
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	stock = Bought_stock.query.filter_by(user_id=1, id=id).first()
	user1.budget = user1.budget + stock.price
	db.session.delete(stock)
	db.session.commit()
	return jsonify({})

@app.route('/buy',methods=['POST'])
def buy():
	user1 = User.query.filter_by(id=1).first()
	if user1.budget >= request.json['price'] :
		bought_stock = Bought_stock(name= request.json['name'], price= request.json['price'], type_achat="normal", user_id=1)
		print(user1.budget,' ',bought_stock.price)
		sd1 = user1.budget
		user1.budget = sd1 - bought_stock.price
		db.session.add(bought_stock)
		db.session.commit()
		return jsonify({"avant" :sd1 , "stock" : bought_stock.price, "apres":sd1 - bought_stock.price})
	else :
		return jsonify({"message": "Not enough money!"})


@app.route('/changeBudget',methods=['PUT'])
def change_budget():
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	user1.budget = request.json['budget']
	db.session.commit()
	return jsonify({'budget':user1.budget})

@app.route('/getBudget',methods=['GET'])
def get_Budget():
	user1 = User.query.filter_by(id=1).first() #Choix de l'utilisateur1
	return jsonify({'budget':user1.budget})


@app.route('/api/market/<string:stock_name>',methods=['GET'])
def gets_stocks(stock_name):
    return jsonify(dts.stacks(stock_name))


@app.route('/stocks/<string:stock_name>',methods=['GET'])
def get_price(stock_name,):
    objet = dts.get_price(stock_name, "2020-07-02")
    return jsonify(objet)


if __name__ == '__main__':
	app.run()
